<?php
 /**
 * Class     MenuRefresh.php
  * @category Bitbull
  * @package  Bitbull_RefreshActiveMenuItem
  * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
  */

class Bitbull_RefreshActiveMenuItem_Block_MenuRefresh extends Mage_Core_Block_Template{

    private $_category;

    public function getCurrentCategory(){
        /** @var Bitbull_RefreshActiveMenuItem_Helper_Data $helper */
        $helper = Mage::helper('bitbull_refreshactivemenuitem');

        if(!$helper->isEnabled()){
            return null;
        }

        return $helper->getCurrentCategory();
    }

    public function isEnable(){
        /** @var Bitbull_RefreshActiveMenuItem_Helper_Data $helper */
        $helper = Mage::helper('bitbull_refreshactivemenuitem');

        if((!$helper->isEnabled() && !Mage::app()->getStore()->isAdmin() ) ||  !$helper->isBlocksCacheEnabled()){
            return false;
        }
        return true;
    }

} 