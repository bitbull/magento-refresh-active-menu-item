<?php
 /**
 * Class     Data.php
 * @category Bitbull
 * @package  Bitbull_RefreshActiveMenuItem
 * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
 */

class Bitbull_RefreshActiveMenuItem_Helper_Data extends Mage_Core_Helper_Abstract{

    const CONFIG_MODULE_ENABLE= 'design/bitbull_refreshactivemenuitem/enabled';
    const CONFIG_ACTIVE_CLASS= 'design/bitbull_refreshactivemenuitem/active_class';
    const CONFIG_MENU_CLASS= 'design/bitbull_refreshactivemenuitem/menu_class';
    const CONFIG_ITEM_MENU_CLASS= 'design/bitbull_refreshactivemenuitem/item_menu_class';

    const CACHE_KEY_MENU ='bitbull_refreshactivemenuitem_menu';

    public function isEnabled(){
        return Mage::getStoreConfigFlag(self::CONFIG_MODULE_ENABLE);
    }

    public function getCurrentCategory(){
        return Mage::registry('current_category');
    }

    public function getActiveClass(){
        return Mage::getStoreConfig(self::CONFIG_ACTIVE_CLASS);
    }

    public function getMenuClass(){
        return Mage::getStoreConfig(self::CONFIG_MENU_CLASS);
    }

    public function convertMenuInData($menu){
        $result = array();
        if(!$menu)
            return;

        $nodes = $menu->getAllChildNodes();
        foreach($nodes as $node){
            $result[$this->_extactIdByNode($node)]=$node->getPositionClass();
            array_merge($result, $this->convertMenuInData($node));
        }

        return $result;
    }

    protected function _extactIdByNode($node){
        $nodeId = $node->getId();
        if($nodeId)
            return str_replace('category-node-','',$nodeId);
        return false;
    }

    public function saveDataMenuInCache($dataMenu){

        Mage::app()->getCache()->save(serialize($dataMenu),self::CACHE_KEY_MENU);

    }

    public function getDataMenu(){
        return unserialize( Mage::app()->getCache()->load(self::CACHE_KEY_MENU));
    }

    public function isBlocksCacheEnabled(){
        return Mage::app()->useCache(Mage_Core_Block_Abstract::CACHE_GROUP);
    }

    public function getItemMenuClass(){
        return Mage::getStoreConfig(self::CONFIG_ITEM_MENU_CLASS);
    }
} 