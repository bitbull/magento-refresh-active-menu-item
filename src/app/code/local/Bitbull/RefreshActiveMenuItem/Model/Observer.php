<?php
 /**
 * Class     Observer.php
  * @category Bitbull
  * @package  Bitbull_RefreshActiveMenuItem
  * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
  */

class Bitbull_RefreshActiveMenuItem_Model_Observer {


    public function savePositionClass($observer){
        $helper= Mage::helper('bitbull_refreshactivemenuitem');

        if(!$helper->isEnabled() || !$helper->isBlocksCacheEnabled())
            return;

        $menu = $observer->getMenu();

        $dataMenu = $helper->convertMenuInData($menu);

        $helper->saveDataMenuInCache($dataMenu);

    }
} 