Bitbull RefreshActiveMenuItem Extension
=====================
Module to add a JavaScript to update active item menu when the cache is active

Description
-----------
Il modulo permette di aggiornare le voci di menu attive senza dover cancellare la cache per ricreare il blocco ogni volta.

Una volta installato bisogna configurare all'interno della sezione "Design" la tab "Refresh Active Menu Item", in questa configurazione dovranno essere impostati le classi per:

  - identificare il menu
  - nome della classe per segnalare l'attivazione dell'item
  - iniziali del nome della classe che indica la posizione del menu

 di default questi valori sono:

  - nav
  - active
  - nav-


Dichiarare il blocco a livello di xml

```
<reference name="header">
    <block type="bitbull_refreshactivemenuitem/menuRefresh" name="bitbull.refreshactivemenuitem.menuRefresh"
                   template="bitbull/refreshactivemenuitem/menu_refresh.phtml"/>
</reference>
```

Aggiungere nel phtml del header.phtml l'aggiunta del nuovo blocco dopo il menu

    ...
    <div class="menu-container">
        <?php echo $this->getChildHtml('topMenu') ?>
        <?php echo $this->getChildHtml('bitbull.refreshactivemenuitem.menuRefresh') ?>
    ...


Licence
-------
Proprietary Bitbull s.r.l.

Copyright
---------
(c) 2015 Bitbull
